#ifndef row_h
#define row_h

struct row
{
	int size;
	const char** values;
};

struct row* init_row(int s, const char* const* v);
void clean_row(struct row* k);
void destroy_row(struct row** k);

void show_row(const struct row* k);
#endif

