#include <stdio.h>
#include <stdlib.h>
#include "row.h"

void init_row_data(struct row* k, int s, const char* const* v)
{
	int i;
	k->size = s;
	k->values = malloc(sizeof(char*) * s);
	for (i = 0; i < s; ++i)
	{
		if (v)
			k->values[i] = v[i];
		else
			k->values[i] = 0;
	}
}

struct row* init_row(int s, const char* const* v)
{
	struct row* k = malloc(sizeof(struct row));
	init_row_data(k, s, v);
	return k;
}

//struct row* init_row(int s)
//{
//	return init_row(s, 0);
//}

void clean_row(struct row* k)
{
	free(k->values);
	k->values = 0;
	k->size = 0;
}

void destroy_row(struct row** k)
{
	clean_row(*k);
	free(*k);
	*k = 0;
}

void show_row(const struct row* k)
{
	int i;
	printf("[");
	for (i = 0; i < k->size; ++i)
	{
		if (k->values[i])
			printf("%s", k->values[i]);
		else
			printf("-");
		if (i + 1 < k->size)
			printf(", ");
	}
	printf("]\r\n");
}

