#ifndef table_h
#define table_h

#include "row.h"

struct table
{
	int size;
	struct row** rows;
};

//void init_table_data(struct table* k, int sx, int sy);
struct table* init_table(int sx, int sy);
void clean_table(struct table* k);
void destroy_table(struct table** k);

void add(struct table* o, struct row* k);

void show_table(const struct table* k);

#endif
