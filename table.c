#include <stdio.h>
#include <stdlib.h>

#include "table.h"

void init_table_data(struct table* k, int sx, int sy)
{
	int i;

	k->size = sy;
	if (sy > 0)
	{
		k->rows = malloc(sizeof(struct row*) * sy);
		for (i = 0; i < sy; ++i)
			k->rows[i] = init_row(sx, 0);
	}
	else
		k->rows = 0;
}

struct table* init_table(int sx, int sy)
{
	struct table* k = malloc(sizeof(struct table));
	init_table_data(k, sx, sy);
	return k;
}

void clean_table(struct table* k)
{
	int i;
	for (i = 0; i < k->size; ++i)
	{
		struct row* r = k->rows[i];
		destroy_row(&r);
		k->rows[i] = r;
	}
	free(k->rows);
	k->size = 0;
	k->rows = 0;
}

void destroy_table(struct table** k)
{
	clean_table(*k);
	free(*k);
	*k = 0;
}

void add(struct table* t, struct row* r)
{
	t->size += 1;
	t->rows = realloc(t->rows, t->size * sizeof(struct row*));
	t->rows[t->size - 1] = r;
}

void show_table(const struct table* k)
{
	int i;
	printf("{\r\n");
	for (i = 0; i < k->size; ++i)
	{
		printf("\t");
		show_row(k->rows[i]);
	}
	printf("}\r\n");
}

