#include <stdio.h>
#include <stdlib.h>

#include "row.h"
#include "table.h"

// funkcja wykonująca kobinację na przekazanej tabeli (zbiorze wierszy o różnych rozmiarach)
struct table* combine(const struct table* input)
{
	int r, i, c, j;
	struct row* current;

	// obliczenie wyjściowej ilości wierszy
	r = 1;
	for (i = 0; i < input->size; ++i)
		r *= input->rows[i]->size;

	// stworzenie tabeli wyjściowej, ilość wartości w każdym wierszu jest stala i jest rowna ilosci przekazanych wierszy
	struct table* out = init_table(input->size, r);

	for (i = 0; i < input->size; ++i)
	{
		// kolejny wiersz wejsciowy
		current = input->rows[i];

		// obliczenie skumulowanej ilosci podleglych wierszy
		c = 1;
		for (j = i+1; j < input->size; ++j)
			c *= input->rows[j]->size;
		
		// uzupelnienie wartosci w tabeli wyjsciowej
		for (j = 0; j < r; ++j)
			out->rows[j]->values[i] = current->values[(j / c) % current->size];
	}
	return out;
}

struct table* create_sample_table();

int main()
{
	struct table* input = create_sample_table();

	show_table(input);

	struct table* combined = combine(input);
	show_table(combined);

	destroy_table(&combined);
	destroy_table(&input);

	return 0;
}

struct table* create_sample_table()
{
	const char* tab1[3] = {"1a", "2a", "3a"};
	const char* tab2[2] = {"1b", "2b"};
	const char* tab3[4] = {"1c", "2c", "3c", "4c"};

	struct table* t = init_table(0, 0);
	add(t, init_row(3, tab1));
	add(t, init_row(2, tab2));
	add(t, init_row(4, tab3));
	return t;
}



